package mx.rulo4.spark.joins;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

public class DatasetSubstraction {

  public static StructType structType1 = new StructType(new StructField[]{
      new StructField("col1", DataTypes.IntegerType, true, Metadata.empty()),
      new StructField("col2", DataTypes.StringType, true, Metadata.empty()),
      new StructField("col3", DataTypes.StringType, true, Metadata.empty()),
  });

  public static StructType structType2 = new StructType(new StructField[]{
      new StructField("col1", DataTypes.IntegerType, true, Metadata.empty()),
      new StructField("col2", DataTypes.StringType, true, Metadata.empty())
  });

  public static void main(String[] args) {
    SparkSession spark = SparkSession.builder().appName("documentation").master("local[4]").getOrCreate();

    List<Row> list1 = new ArrayList<>();
    list1.add(RowFactory.create(1, "g2", "a2"));
    list1.add(RowFactory.create(2, "g3", "a3"));
    list1.add(RowFactory.create(3, "g3", "a3"));
    list1.add(RowFactory.create(4, "g4", "a4"));
    list1.add(RowFactory.create(5, "g5", "a5"));

    List<Row> list2 = new ArrayList<>();
    list2.add(RowFactory.create(1, "g2"));
    list2.add(RowFactory.create(2, "g3"));
    list2.add(RowFactory.create(3, "g3"));

    Dataset<Row> dataset1 = spark.createDataset(list1, RowEncoder.apply(structType1));
    Dataset<Row> dataset2 = spark.createDataset(list2, RowEncoder.apply(structType2));

    dataset1.join(dataset2, dataset1.col("col1").equalTo(dataset2.col("col1")), "leftanti").show(false);
  }

}
