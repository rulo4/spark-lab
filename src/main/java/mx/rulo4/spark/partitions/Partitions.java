package mx.rulo4.spark.partitions;

import org.apache.commons.lang3.RandomUtils;
import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Partitions {

    public static StructType structType = new StructType(new StructField[]{
            new StructField("id", DataTypes.IntegerType, true, Metadata.empty()),
            new StructField("group", DataTypes.StringType, true, Metadata.empty()),
            new StructField("account", DataTypes.StringType, true, Metadata.empty()),
            new StructField("code", DataTypes.StringType, true, Metadata.empty()),
            new StructField("date", DataTypes.TimestampType, true, Metadata.empty()),
    });

    public static void main(String[] args) {

        SparkSession spark = SparkSession.builder().appName("documentation").master("local[4]").getOrCreate();

        List<Row> dataList = new ArrayList<Row>();
        dataList.add(RowFactory.create(1 , "g2", "a2", "CODE1", Timestamp.valueOf("2020-01-01 14:00:00")));
        dataList.add(RowFactory.create(2 , "g3", "a3", "CODE1", Timestamp.valueOf("2020-01-01 12:00:00")));
        dataList.add(RowFactory.create(3 , "g3", "a3", "CODE1", Timestamp.valueOf("2020-01-01 14:00:00")));
        dataList.add(RowFactory.create(4 , "g4", "a4", "CODE1", Timestamp.valueOf("2020-01-01 14:00:00")));
        dataList.add(RowFactory.create(5 , "g5", "a5", "CODE1", Timestamp.valueOf("2020-01-01 12:00:00")));
        dataList.add(RowFactory.create(6 , "g4", "a4", "CODE1", Timestamp.valueOf("2020-01-01 12:00:00")));
        dataList.add(RowFactory.create(7 , "g1", "a1", "CODE1", Timestamp.valueOf("2020-01-01 12:00:00")));
        dataList.add(RowFactory.create(8 , "g1", "a1", "CODE1", Timestamp.valueOf("2020-01-01 14:00:00")));
        dataList.add(RowFactory.create(9 , "g2", "a2", "CODE1", Timestamp.valueOf("2020-01-01 12:00:00")));
        dataList.add(RowFactory.create(10, "g5", "a5", "CODE1", Timestamp.valueOf("2020-01-01 14:00:00")));


        Dataset<Row> dataset = spark.createDataset(dataList, RowEncoder.apply(structType));
//        printByPartition(dataset, "dataset");
//        writeDataset(dataset, "dataset");

        Dataset<Row> groupedBy = dataset.groupBy("group", "account").count();
        printByPartition(groupedBy, "groupedBy");
        writeDataset(groupedBy, "groupedBy");

        Dataset<Row> repartitionedNum = dataset.repartition(2);

        Dataset<Row> repartitionedCol = dataset.repartition(dataset.col("group"), dataset.col("account"));

        spark.close();
    }

    public static void printByPartition(Dataset<Row> dataset, String name) {
        System.out.println(name + "Partitions: " + dataset.toJavaRDD().getNumPartitions());

        dataset.mapPartitions((MapPartitionsFunction<Row, Row>) partition -> {
            int partitionId = RandomUtils.nextInt(0, 500);
            List newPartition = new ArrayList();
            while (partition.hasNext()) {
                Row row = partition.next();
                StringBuilder str = new StringBuilder("[P ");
                Arrays.stream(row.schema().fieldNames()).forEach(field ->
                        str.append(partitionId).append(", ").append(row.getAs(field).toString())
                );
                str.append("]");

                System.out.println(str);

                newPartition.add(row);
            }
            return newPartition.iterator();
        }, RowEncoder.apply(structType)).count();
    }

    public static void writeDataset(Dataset<Row> dataset, String name) {
        dataset.write().csv("src/main/resources/output/" + name);
    }
}
