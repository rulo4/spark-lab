package mx.rulo4.spark.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SelectDistinct {
  
  public static void main(String[] args) {
    SparkSession sparkSession = SparkSession.builder().appName("documentation").master("local[4]").getOrCreate();

    String textFile = "/home/raul/Documents/onvision/sparktests/src/main/resources/input/distinct.csv";
    Dataset<Row> readDataset = sparkSession.sqlContext().read()
        .format("CSV")
        .option("header", "true")
        .load(textFile);
    
    Dataset<Row> distinctCol3 = readDataset.select("col3").distinct();
    
    distinctCol3.show(100, false);
    
  }
}
