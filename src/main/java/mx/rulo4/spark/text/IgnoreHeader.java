package mx.rulo4.spark.text;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class IgnoreHeader {
  
  public static void main(String[] args) {
    SparkSession sparkSession = SparkSession.builder().appName("documentation").master("local[4]").getOrCreate();

    String textFile = "/home/raul/Documents/onvision/sparktests/src/main/resources/input/textWithHeader.txt";    
    Dataset<Row> readDataset = sparkSession.sqlContext().read()
        .format("CSV")
        .option("header", "true")
        .load(textFile);
    
    readDataset.show(100, false);
  }
}
